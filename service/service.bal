import ballerina/io;
import ballerinax/kafka;
import ballerina/http;


// Design and implement a distributed storage system
// We have a client and multiple instances of a server
// The system supports two operations: Read and Write object
// Each object is in JSON format and stored in a local file for each instance of the server
// The file has a limited size of 30MB. 
// Each object has a key(string) attached to it. 
// We use Kafka has a middleware between the client and servers
// To write an object, the JSON content and the key are sent to the server through Kafka.
// When the write operation succeeds, the client receives an ACK from each server.
// A write operation with existing key replaces the existing object (Update)
// The client sends a request through Kafka and receives a copy from each server with the logical clock attached to it to read an object.
// The client then analyzes the logical clocks, decides upon the latest version and prints it out.
// If there are multiple concurrent versions, it prints them all out.
// If the client identifies a version older than others, it notifies the replica to then update the store.

kafka:ConsumerConfiguration consumerConfiguration = {
    groupId: "group-id",
    offsetReset: "earliest",
    topics: ["storage-system"]
};

kafka:Consumer consumer = check new (kafka:DEFAULT_URL, consumerConfiguration);

string filePath = "./files/storage-system.json";
json dataFile = check io:fileReadJson(filePath);
map<json[]> storageMap = check dataFile.cloneWithType();

json[] students = storageMap.get("students");

service /storage on new http:Listener(8080) {
    resource function get write() returns json|error{
        kafka:ConsumerRecord[] records = check consumer->poll(1);

        foreach var kafkaRecord in records {
        byte[] messageContent = kafkaRecord.value;
        string message = check string:fromBytes(messageContent);
        students.push(message.toJson());
        storageMap["students"] = students;
        check io:fileWriteJson(filePath, storageMap);

        }
        
        return {message: "Message received"};

    }

    resource function get read () returns json|error{
        
    }
}

