import ballerina/io;
import ballerinax/kafka;
import ballerina/http;

kafka:ProducerConfiguration storageConfig = {
    clientId: "storage-producer",
    acks: "all",
    retryCount: 3
};

kafka:Producer storageProducer = check  new (kafka:DEFAULT_URL, storageConfig);


public function main() returns error?{

    json message = {
        "firstname": "Braulio",
        "lastname": "Andre"
    };
    string key = "219036810";

    check storageProducer->send({
        topic: "storage-system",
        key: key.toBytes(),
        value:  message.toJsonString().toBytes()

    });

    check storageProducer->'flush();
    
    final http:Client clientEndpoint = check new("http://localhost:8080/storage");

    io:println("GET response from server");

    json resp = check clientEndpoint->get("/write");

    io:println(resp.toJsonString());

}